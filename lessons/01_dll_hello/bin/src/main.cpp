﻿#include "lib.hpp"

int main(int argc, char* argv[])
{    
    std::string username = "artem";  
	if (argc > 1) 
	{        
        username = argv[1];
    }
    int exitCode = greetings(username) ? EXIT_SUCCESS : EXIT_FAILURE;

    return exitCode;
}