#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.hpp"

int main()
{
	std::unique_ptr<myns::engine, void(*)(myns::engine*)> engine(
		myns::create_engine(), myns::destroy_engine);

	std::string err = engine->initialize("");
	if (!err.empty())
	{
		std::cerr << err << std::endl;
		return EXIT_FAILURE;
	}

	bool continue_loop = true;
	while (continue_loop)
	{
		myns::event event;

		while (engine->read_input(event))
		{
			std::cout << event << std::endl;
			switch (event)
			{
			case myns::event::turn_off:
				continue_loop = false;
				break;
			default:
				break;
			}
		}
	}

	engine->uninitialize();

	return EXIT_SUCCESS;
}
